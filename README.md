# README #

This document provides a quick overview of what this build script is for and how to integrate it into your own workflow.

### RapidWeaver Addon Build Script ###

* Variables are setup at the beginning of the file (file paths, product version, product name, etc)
* These variables are used throughout to move things in place and create files
* Your workflow and file organization are likely different than mine, so you'll probably want to make adjustments

### What does this script do exactly? ###

1. Make a release version of the product (excluding git repo files) and store it where you specify locally
2. Make a zip file, appending the version number, sign it for Sparkle, then upload it to your specified server
3. Create a dmg file via DropDMG's cli (if you don't use DropDMG, just comment out that part in the script) and then backup the dmg, release version and zip file to where you want it archived

### Roadmap ###

1. Publish the release file to S3, so that services like Cartloom will point to the latest version automatically
2. Tweet about the release

Other suggestions welcome.

### How do I get set up? ###

1. Add this build script file to each product, in the same directory as the "Contents" folder for each theme/stack/plugin.

2. Make the file executable with this terminal command: `chmod +x build.sh`

3. To run the build script open a terminal session, navigate to the directory where the build script is located, then enter this command: `./build.sh ProductType ProductName`

For example, `./build.sh stack Charts`

Or, `./build.sh theme Mirage`