#!/bin/bash
set -e

TOOLVERSION=1.0
TOOLDATE=""

if [ -z "$1" ]
  then
    echo "product type not supplied"
    exit 1
fi

# Product Development Directory
DEVDIR=`pwd`
# Grab the filename from the file path
FILENAME=`basename "${DEVDIR}"`
# Get the file extension
FILEEXTENSION=${DEVDIR##*.}
# These two variable grab the input when the script is invoked
PRODUCTTYPE=$1
PRODUCTNAME=$2
# Run check if product name isn't provided when script is called
if [ -z "$2" ]
    then
    PRODUCTNAME=`defaults read "${DEVDIR}/Contents/Info.plist" CFBundleIdentifier`
    PRODUCTNAME=${PRODUCTNAME##*.}
    PRODUCTNAME=`echo "${PRODUCTNAME}" | awk '{for(i=1;i<=NF;i++)sub(/./,toupper(substr($i,1,1)),$i)}1'`
fi
#Get the product version and build number from the Info.plist file
PRODUCTVERSION=`defaults read "${DEVDIR}/Contents/Info.plist" CFBundleShortVersionString`
PRODUCTBUILDNUMBER=`defaults read "${DEVDIR}/Contents/Info.plist" CFBundleVersion`

# The backup directory where you want the files archived
PRODUCTBACKUPDIR="/Local/file/path/where/you/want/product/archived"

# The local release directory
RELEASEDIR="/Local/file/path/where/you/keep/release/versions"
# The remote release direcotry
REMOTERELEASEDIR="/Remote/file/path/on/your/server"

# Sparkle related information
SPARKLESIGNINGSCRIPTPATH="/Local/file/path/to/your/sparkle/signing/script"
SPARKLEPRIVKEYPATH="/Local/file/path/to/your/private/key"
SPARKLEDSASIGNATURE=
SPARKLEUPDATEFILESIZE=

# The URL (directory, not a specific file name) where updates should be uploaded
UPDATEURL="http://website_url.com/directory/where/updates/are/stored"

# File Organization - store product type in relevant directory
if [[ $PRODUCTTYPE == "stack" ]]
    then
    UPDATEURL="$UPDATEURL/stacks"
    RELEASEDIR="$RELEASEDIR/Stacks/${PRODUCTNAME}"
    REMOTERELEASEDIR="$REMOTERELEASEDIR/stacks"
    PRODUCTBACKUPDIR="$PRODUCTBACKUPDIR/Stacks/${PRODUCTNAME}/Releases"
fi
if [[ $PRODUCTTYPE == "theme" ]]
    then
    UPDATEURL="$UPDATEURL/themes"
    RELEASEDIR="$RELEASEDIR/Themes/${PRODUCTNAME}"
    REMOTERELEASEDIR="$REMOTERELEASEDIR/themes"
    PRODUCTBACKUPDIR="$PRODUCTBACKUPDIR/Themes/${PRODUCTNAME}/Releases"
fi
if [[ $PRODUCTTYPE == "plugin" ]]
    then
    RELEASEDIR="$RELEASEDIR/Plugins/${PRODUCTNAME}"
    PRODUCTBACKUPDIR="$PRODUCTBACKUPDIR/Plugins/${PRODUCTNAME}/Releases"
fi

SPARKLEPUBDATE=`LC_TIME=en_US date +"%a, %d %b %G %T %z"`

# Get the previous release version number
PREVIOUSRELEASE=`ls -1 ${PRODUCTBACKUPDIR} | sort -r | head -n1`

# This portion actually runs the functions defined below
auto_init () {
    # Run functions
    createReleaseFile
    prepareAndUploadSparkleUpdate
    createDMGFile

}

# Create release file (.rwtheme/.rapidweavertheme, .stack, .rwplugin/.rapidweaverplugin)
# Exclude git files and other extra stuff (this build script) from release version
createReleaseFile() {
    echo "Creating release file at ${RELEASEDIR} path..."
    rm -rf "${RELEASEDIR}"
    rsync -r --exclude=.git/ --exclude=.gitignore --exclude=git.log.format --exclude=README.* --exclude=build.sh --exclude=*release.md --exclude=*release-history.txt --exclude=.DS_Store "${DEVDIR}" "${RELEASEDIR}"
    echo "Done!"
}


# - Upload compressed file to site
# - Upload appcast file
# TODO - Upload release notes
prepareAndUploadSparkleUpdate() {
    # 1. Compress release file
    echo "Compressing file into archive for Sparkle updating..."
    pushd ${RELEASEDIR}
    zip -rq ${PRODUCTNAME}-v${PRODUCTVERSION}.zip ${FILENAME}
    popd
    echo "Done!"
    # 2. Sign compressed file with private key for Sparkle updates
    echo "Signing update file..."
    SPARKLEDSASIGNATURE=`${SPARKLESIGNINGSCRIPTPATH} ${RELEASEDIR}/${PRODUCTNAME}-v${PRODUCTVERSION}.zip ${SPARKLEPRIVKEYPATH}`
    echo "Done!"
    # 3. Get file size
    echo "Calculating file size..."
    SPARKLEUPDATEFILESIZE=`stat -f %z ${RELEASEDIR}/${PRODUCTNAME}-v${PRODUCTVERSION}.zip`
    echo "Done!"
    # 4. Create appcast.xml/rw6appcast.xml file
    echo "Creating appcast file..."
# Don't indent the lines below, otherwise an error will occur and the appcast file won't get created
(cat << ITEM
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:sparkle="http://www.andymatuschak.org/xml-namespaces/sparkle" xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
<title>The $PRODUCTNAME $PRODUCTTYPE from NimbleHost</title>
<link>$APPCASTURL/appcast.xml</link>
<description>Most recent changes with links to updates.</description>
<language>en</language>
<item>
    <title>$PRODUCTNAME v$PRODUCTVERSION</title>
    <sparkle:releaseNotesLink>$UPDATEURL/$PRODUCTNAME/releaseNotes.html</sparkle:releaseNotesLink>
    <pubDate>$SPARKLEPUBDATE</pubDate>
    <enclosure
        url="$UPDATEURL/$PRODUCTNAME/$PRODUCTNAME-v$PRODUCTVERSION.zip"
        sparkle:shortVersionString="$PRODUCTVERSION"
        sparkle:version="$PRODUCTBUILDNUMBER"
        length="$SPARKLEUPDATEFILESIZE"
        sparkle:dsaSignature="$SPARKLEDSASIGNATURE"
        type="application/octet-stream"
    />
</item>
</channel>
</rss>
ITEM
) > ${RELEASEDIR}/appcast.xml;
    echo "Done!"
    ### TODO ###
    # Create and upload release notes
    ###

    # 6. Upload all necessary files for Sparkle updates
    echo "Uploading files for Sparkle updates..."
    rsync -r --exclude=.DS_Store --exclude=${FILENAME} $RELEASEDIR your_username@your_domain.com:$REMOTERELEASEDIR
    echo "Done!"
}

# Create the actual download file users will get when purchasing the product
# This function uses DropDMG's cli. Remove/replace if needed for your own workflow.
createDMGFile() {
    # Create folder of required files. For NimbleHost, this means the release version, a folder called "Extras", and link to documentation
    echo "Preparing files to create DMG for customers..."
    rsync -r --exclude=${FILENAME} --exclude=${PRODUCTNAME}-${PREVIOUSRELEASE}.zip --exclude=${PRODUCTNAME}-${PREVIOUSRELEASE}.dmg "${PRODUCTBACKUPDIR}/${PREVIOUSRELEASE}/" "${PRODUCTBACKUPDIR}/v${PRODUCTVERSION}"
    rsync -r "${RELEASEDIR}/${FILENAME}" "${PRODUCTBACKUPDIR}/v${PRODUCTVERSION}"
    echo "Done!"
    # Actually create the DMG file
    echo "Creating DMG file..."
    PATHTODMGFILE=`dropdmg --layout-name=${PRODUCTNAME} --base-name=${PRODUCTNAME}-v${PRODUCTVERSION} --volume-name=${PRODUCTNAME}\ v${PRODUCTVERSION} --destination=${PRODUCTBACKUPDIR} ${PRODUCTBACKUPDIR}/v${PRODUCTVERSION}`
    # Move the DMG file into the directory where the other release files are stored
    echo "Moving DMG file into place..."
    mv $PATHTODMGFILE ${PRODUCTBACKUPDIR}/v${PRODUCTVERSION}
    echo "Done!"
}

toolUsage () {
less << EOF1
Copyright (c) 2014 Jonathan Head
build tool ${TOOLVERSION} (${TOOLDATE}).

Usage:

    ./build [-options] [-h]

OPTIONS:
    -h  Show this message
    
    
EXAMPLES:
    
    ./build Theme Mirage
        This will specify the product type as Theme and the product name as Mirage.
        
EOF1
}

##################
# RUN THE SCRIPT #
##################
# get arguments
while getopts ":htn:v" OPTION
do
    case $OPTION in
        h) toolUsage; exit 1;;
        v) echo "${TOOLVERSION} - ${TOOLDATE}"; exit 1;;
        ?) echo "Invalid option: -$OPTARG"; exit 1;;
    esac
done

auto_init
